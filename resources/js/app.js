require('./bootstrap');

require('moment');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';

Vue.mixin({ methods: { route } });
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);

const app = document.getElementById('app');

const modulesJson = require("../../modules_statuses.json");
var modulesObject = Object.keys(modulesJson);

// @wogan

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => {
                    for(let i = 0; i < modulesObject.length; i++){
                        require('../../Modules/'+modulesObject[i]+'/Resources/assets/Pages/'+name).default
                    }
                }
            },
        }),
}).$mount(app);
